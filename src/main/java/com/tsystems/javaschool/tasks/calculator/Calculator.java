package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result = null;
        ArrayList<Element> inFixForm = new ArrayList<>();

        // checking that expression is not empty
        if (statement == null || statement.isEmpty()) {
            return null;
        }

        // checking all symbols and numbers or allowed math operations
        if (!statement.matches("^[0-9*\\-+().\\/]+$")) {
            return null;
        }

        // splitting string by strings and iterate by elements
        Element lastAddedElement = null;
        String[] inFix = statement.split("");
        for (String inFixElement : inFix) {
            // getting type of current element
            ElementType elementType = getTypeToElement(inFixElement);
            if (elementType == null) {
                // unknown symbol
                return null;
            }

            // concating strings if this is a number greater than 9 or a floating-point number
            if ((elementType == ElementType.NUMBER || elementType == ElementType.DOT)
                    && lastAddedElement != null &&
                    (lastAddedElement.getElementType() == ElementType.NUMBER ||
                            lastAddedElement.getElementType() == ElementType.DOT)) {
                // concating the last and the current element
                lastAddedElement.addSymbol(inFixElement);
            } else {
                // creating a new element and add it to the list of the elements
                lastAddedElement = new Element(inFixElement, elementType);
                inFixForm.add(lastAddedElement);
            }
        }

        // checking errors in the expression
        if (!checkExpressionCorrect(inFixForm)) {
            return null;
        }

        // changing the expression: add "*" after the number and before the bracket
        // or after the bracket and before the number
        // or between brackets
        for (int i = 0; i < inFixForm.size(); i++) {
            if (i != 0 && inFixForm.get(i).getSymbol().equals("(")
                    && inFixForm.get(i - 1).getElementType() == ElementType.NUMBER) {
                inFixForm.add(i, new Element("*", getTypeToElement("*")));
            } else if (i != inFixForm.size() - 1 && inFixForm.get(i).getSymbol().equals(")")
                    && inFixForm.get(i + 1).getElementType() == ElementType.NUMBER) {
                inFixForm.add(i + 1, new Element("*", getTypeToElement("*")));
            } else if (i != 0 && inFixForm.get(i).getSymbol().equals("(")
                    && inFixForm.get(i - 1).getSymbol().equals(")")) {
                inFixForm.add(i, new Element("*", getTypeToElement("*")));
            }
        }

        // transforming the infixform to the postfixform
        LinkedList<Element> output = getPostFixForm(inFixForm);

        // computing the expression
        result = computePostfixExpression(output);

        return result;
    }

    /**
     * Returns false if the expression is incorrect.
     */
    public boolean checkExpressionCorrect(ArrayList<Element> inFixForm) {
        if (inFixForm == null || inFixForm.isEmpty()) {
            return false;
        }

        // checking allowed symbols for the first element
        Element firstElement = inFixForm.get(0);
        if (firstElement.getSymbol().equals("+")
                || firstElement.getSymbol().equals("*")
                || firstElement.getSymbol().equals("/")
                || firstElement.getSymbol().equals(".")) {
            return false;
        }

        // checking allowed symbols for the last element
        Element lastElement = inFixForm.get(inFixForm.size() - 1);
        if (lastElement.getElementType() != ElementType.PARENTHESES
                && lastElement.getElementType() != ElementType.NUMBER) {
            return false;
        }

        // checking that after an operator a number goes or parentheses
        for (int i = 0; i < inFixForm.size(); i++) {
            Element currentElement = inFixForm.get(i);

            if (i != inFixForm.size() - 1) {
                Element nextElement = inFixForm.get(i + 1);
                if (currentElement.getElementType() == ElementType.OPERATOR
                        & nextElement.getElementType() == ElementType.OPERATOR) {
                    return false;
                }
            }
        }

        // checking parentheses
        Stack<String> stackOfParentheses = new Stack<>();
        for (int i = 0; i < inFixForm.size(); i++) {
            if (inFixForm.get(i).getSymbol().equals("(")) {
                stackOfParentheses.push("(");
                continue;
            }
            if (inFixForm.get(i).getSymbol().equals(")")) {
                if (stackOfParentheses.isEmpty()) {
                    return false;
                } else {
                    String theLast = stackOfParentheses.peek();
                    if (inFixForm.get(i).getSymbol().equals(")") && theLast.equals("(")) {
                        stackOfParentheses.pop();
                    } else {
                        return false;
                    }
                }
            }
        }
        if (!stackOfParentheses.isEmpty()) {
            return false;
        }

        // checking floating-point numbers
        for (Element currentElement : inFixForm) {
            if (currentElement.getSymbol().length() > 1) {
                String[] arr = currentElement.getSymbol().split("");
                int countOfDots = 0;
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i].equals(".")) {
                        countOfDots++;
                        if (countOfDots > 1) {
                            return false;
                        }
                    }
                }
            }
        }

        // eliminating ArithmeticException
        for (int i = 0; i < inFixForm.size(); i++) {
            if (inFixForm.get(i).getSymbol().equals("/") && inFixForm.get(i + 1).getSymbol().equals("0")) {
                return false;
            }
        }
        return true;
    }

    /**
     * Transforms infixform to postfixform for further computing
     */
    public LinkedList<Element> getPostFixForm(ArrayList<Element> inFix) {
        LinkedList<Element> operatorStack = new LinkedList<>();
        LinkedList<Element> output = new LinkedList<>();

        for (Element token : inFix) {
            if (token.getElementType() == ElementType.NUMBER) {
                output.addLast(token);
            } else if (token.getSymbol().equals("(")) {
                operatorStack.addLast(token);
            } else if (token.getElementType() == ElementType.OPERATOR) {

                while (!operatorStack.isEmpty() && operatorStack.peekLast().getPrecedence() >= token.getPrecedence()) {
                    output.addLast(operatorStack.pollLast());
                }
                operatorStack.addLast(token);

            } else if (token.getSymbol().equals(")")) {
                while (!operatorStack.peekLast().getSymbol().equals("(")) {
                    output.addLast(operatorStack.pollLast());
                }
                operatorStack.pollLast();
            }
        }
        while (!operatorStack.isEmpty()) {
            output.addLast(operatorStack.pollLast());
        }
        return output;
    }

    /**
     * Computes postfixform Stack and returns the answer as a String
     */
    public String computePostfixExpression(LinkedList<Element> stack) {
        LinkedList<Double> stackForComputing = new LinkedList<>();
        for (Element token : stack) {
            if (token.getElementType() == ElementType.NUMBER) {
                stackForComputing.addLast(Double.parseDouble(token.getSymbol()));
            } else {
                double secondOperand = stackForComputing.pollLast();
                double firstOperand = 0;
                if (!stackForComputing.isEmpty()) {
                    firstOperand = stackForComputing.pollLast();
                }
                double result = 0;
                switch (token.getSymbol()) {
                    case "*":
                        result = firstOperand * secondOperand;
                        break;
                    case "+":
                        result = firstOperand + secondOperand;
                        break;
                    case "/":
                        if (secondOperand == 0) {
                            return null;
                        } else {
                            result = firstOperand / secondOperand;
                            break;
                        }
                    case "-":
                        result = firstOperand - secondOperand;
                        break;
                }
                stackForComputing.addLast(result);
            }
        }

        double result = stackForComputing.pollLast();

        // rounding the answer and discarding redundant numbers, if the answer is a whole number
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.HALF_UP);
        String rounded = df.format(result);
        rounded = rounded.replace(',', '.');

        return rounded;
    }

    /**
     * Assigning a type to each element(number, operator, parentheses, dot)
     */
    public ElementType getTypeToElement(String element) {
        ArrayList<String> numbers = new ArrayList<String>(
                Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));

        ArrayList<String> symbols = new ArrayList<String>(
                Arrays.asList("+", "-", "*", "/"));

        if (numbers.contains(element)) {
            return ElementType.NUMBER;
        } else if (symbols.contains(element)) {
            return ElementType.OPERATOR;
        } else if (element.equals("(") || element.equals(")")) {
            return ElementType.PARENTHESES;
        } else if (element.equals(".")) {
            return ElementType.DOT;
        } else {
            return null;
        }
    }
}
