package com.tsystems.javaschool.tasks.calculator;

public class Element {

    private final ElementType elementType;
    private int precedence = 0;
    private String symbol;

    public Element(String symbol, ElementType elementType) {
        this(symbol, elementType, 0);
    }

    public Element(String symbol, ElementType elementType, int precedence) {
        this.symbol = symbol;
        this.elementType = elementType;
        this.precedence = precedence;
        if (elementType == ElementType.OPERATOR) {
            calculatePrecedence();
        }
    }

    public void addSymbol(String newSymbol) {
        symbol += newSymbol;
    }

    public int getPrecedence() {
        return precedence;
    }

    public ElementType getElementType() {
        return elementType;
    }

    public String getSymbol() {
        return symbol;
    }

    private void calculatePrecedence() {
        switch (symbol) {
            case "+":
            case "-": {
                precedence = 1;
                break;
            }
            case "*":
            case "/": {
                precedence = 2;
                break;
            }
        }
    }
}
