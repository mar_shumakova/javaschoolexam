package com.tsystems.javaschool.tasks.calculator;

public enum ElementType {
    NUMBER,
    OPERATOR,
    PARENTHESES,
    DOT
}
