package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checking if the second list contains the first list in particular order
     *
     * @param list1 the list, that we try to find
     * @param list2 the list, inside which we try to find the first list
     * @return
     */
    public boolean find(List<Object> list1, List<Object> list2) {
        if (list1 == null || list2 == null) {
            throw new IllegalArgumentException();
        }

        // checking if the first list is empty
        if (list1.isEmpty()) {
            return true;
        }

        // checking lists for correct content
        if (!checkLists(list1, list2)) {
            return false;
        }

        // checking if list2 contains list1
        int lastEqualPosition = 0;

        for (int i = 0; i < list1.size(); i++) {
            for (int j = lastEqualPosition; j < list2.size(); j++) {
                if (!list2.contains(list1.get(i))) {
                    return false;
                } else {
                    if (list1.get(i).equals(list2.get(j))) {
                        lastEqualPosition = j;
                        break;
                    } else if (j == list2.size() - 1) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Checking two lists for correct content
     *
     * @param list1 first list to check
     * @param list2 second list to check
     * @return true if lists are correct otherwise false
     */
    public static boolean checkLists(List<Object> list1, List<Object> list2) {
        if (list2.isEmpty()) {
            return false;
        } else {
            return list1.size() <= list2.size();
        }
    }
}
