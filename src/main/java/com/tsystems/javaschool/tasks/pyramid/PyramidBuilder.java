package com.tsystems.javaschool.tasks.pyramid;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // checking if the list is empty or contains null
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        // checking if the size of the list is correct for building the pyramid(triangle number)
        BigDecimal triangleNumberCriteria = BigDecimal.valueOf(8)
                .multiply(BigDecimal.valueOf(inputNumbers.size()))
                .add(BigDecimal.valueOf(1));

        double residueOfDivision = Math.sqrt(triangleNumberCriteria.doubleValue()) % 1;
        if (residueOfDivision > 0) {
            throw new CannotBuildPyramidException();
        }

        // sorting the list
        Collections.sort(inputNumbers);

        // computing the size of the pyramid(using the formula of the roots of quadratic function)
        int countOfArrays = (int) ((-1 + Math.sqrt(triangleNumberCriteria.doubleValue())) / 2);
        int countOfElements = countOfArrays * 2 - 1;

        int[][] result = new int[countOfArrays][countOfElements];

        // building the pyramid
        int posOfNumberToPut = 0;
        for (int row = 0; row < result.length; row++) {
            int posFirstNumber = countOfArrays - row - 1;
            int countOfNumbers = row + 1;
            for (int j = posFirstNumber; countOfNumbers > 0; j = j + 2) {
                result[row][j] = inputNumbers.get(posOfNumberToPut);
                posOfNumberToPut++;
                countOfNumbers--;
            }
        }
        return result;

    }
}

